package com.ms.parser.controller;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.ms.parser.configuration.Model;



 
@RestController
public class HelloWorldRestController {
 
	@Bean(name = "multipartResolver")
	public CommonsMultipartResolver createMultipartResolver() {
	    CommonsMultipartResolver resolver=new CommonsMultipartResolver();
	    resolver.setDefaultEncoding("utf-8");
	    return resolver;
	}
    
	private static final String UPLOAD_DIRECTORY ="/images";
	private static final int THRESHOLD_SIZE     = 1024 * 1024 * 50;  // 3MB
	
    @SuppressWarnings("unused")
	@RequestMapping(value = "/parse", method = RequestMethod.POST, consumes="multipart/form-data")
    public List<Model> parse(HttpServletRequest request, 
    		@RequestParam(value = "fileUpload") MultipartFile fileUpload) throws Exception {
    	
    	System.out.println();
    	MultipartFile aFile = fileUpload; 
            	String filename = aFile.getOriginalFilename();  
            	String path = "C:\\temp";
                System.out.println("Saving file: " + aFile.getOriginalFilename());
                byte[] data = aFile.getBytes();
                BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(  
                        new File(path + File.separator + "file.xlsx")));  
                   stream.write(data);  
                   stream.flush();  
                   stream.close();              
      
                   File myFile = new File("C://temp/file.xlsx"); 
                   FileInputStream fis = new FileInputStream(myFile); 
                   // Finds the workbook instance for XLSX file 
                   XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX 
                   XSSFSheet mySheet = myWorkBook.getSheetAt(0); 
                   List<Model> obj = new ArrayList<>();
                   Iterator<Row> rowIterator = mySheet.iterator();
                   List<String> header = new ArrayList<>();
                   Model m = new Model();
                   int c =0;
                   Row headRow = null;
                   while (rowIterator.hasNext()) {
                       Row row = rowIterator.next();
                       
                       if(c==0){
                    	   headRow = row; 
                       }
                       HashMap<String,String> hm=new HashMap<String,String>();
                   Iterator<Cell> cellIterator = row.cellIterator();
                   int ind =0;
                   System.out.println(row.getCell(0).toString());
                   
                   while (cellIterator.hasNext()) {                	   
                	   
                	   Cell cell = cellIterator.next(); 
                	   System.out.println(cell.getRowIndex());
                	   if(c!=0){
                		   switch (cell.getCellType()) { 
                      	   	case Cell.CELL_TYPE_STRING: 
                      	   		hm.put(headRow.getCell(ind)+"", cell.getStringCellValue());
                      	   		break;
                      	   	case Cell.CELL_TYPE_NUMERIC: 
                      	   	hm.put(headRow.getCell(ind)+"", cell.getNumericCellValue()+"");
                      	   		break;
                      	   	case Cell.CELL_TYPE_BOOLEAN: 
                      	   	hm.put(headRow.getCell(ind)+"", cell.getBooleanCellValue()+"");
                      	   		break;
                      	   	default : } 
                	   
                	   }
                	  ind++;
                	   }
                   if(!hm.isEmpty())
                	   obj.add(new Model(hm));
            	   c++;
                   }

        return obj;
    }

  
 
     
    
 
}