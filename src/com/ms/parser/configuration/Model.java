package com.ms.parser.configuration;

import java.util.HashMap;

public class Model {

	String A;
	String B;
	String C;
	String D;
	
	
	
	public Model(HashMap<String,String> hm) {
		this.A= hm.get("A");
		this.B= hm.get("B");
		this.C= hm.get("C");
		this.D= hm.get("D");
	}
	public Model() {
		super();
	}
	/**
	 * @return the a
	 */
	public String getA() {
		return A;
	}
	/**
	 * @param a the a to set
	 */
	public void setA(String a) {
		A = a;
	}
	/**
	 * @return the b
	 */
	public String getB() {
		return B;
	}
	/**
	 * @param b the b to set
	 */
	public void setB(String b) {
		B = b;
	}
	/**
	 * @return the c
	 */
	public String getC() {
		return C;
	}
	/**
	 * @param c the c to set
	 */
	public void setC(String c) {
		C = c;
	}
	/**
	 * @return the d
	 */
	public String getD() {
		return D;
	}
	/**
	 * @param d the d to set
	 */
	public void setD(String d) {
		D = d;
	}
	
}
